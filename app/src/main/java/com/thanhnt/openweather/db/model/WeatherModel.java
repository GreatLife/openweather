package com.thanhnt.openweather.db.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.thanhnt.openweather.db.DateConverter;
import com.thanhnt.openweather.db.WeatherConverter;

import java.util.Date;
import java.util.List;

/**
 * Created by xedap on 5/10/2018.
 */

@Entity
public class WeatherModel {
    @PrimaryKey
    public long id;
//    @TypeConverters({DateConverter.class})
    public long dt;
    public String name;
    public int visibility;
    @Embedded(prefix = "coord_")
    public Coord coord;
    @Embedded(prefix = "sys_")
    public Sys sys;
    @TypeConverters({WeatherConverter.class})
    public List<Weather> weather;
    @Embedded(prefix = "main_")
    public Main main;
    @Embedded(prefix = "wind_")
    public Wind wind;
    @Embedded(prefix = "cloud_")
    public Cloud cloud;
}
