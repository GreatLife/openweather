package com.thanhnt.openweather.db.model;

/**
 * Created by xedap on 5/10/2018.
 */

public class Coord {
    public double lon;
    public double lat;
}
