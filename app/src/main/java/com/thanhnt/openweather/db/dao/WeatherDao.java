package com.thanhnt.openweather.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.thanhnt.openweather.db.model.WeatherModel;

import java.util.List;

/**
 * Created by xedap on 5/10/2018.
 */

@Dao
public interface WeatherDao {

    @Query("SELECT * FROM weathermodel")
    LiveData<List<WeatherModel>> loadWeathers();

    @Query("SELECT * FROM weathermodel WHERE id=:id")
    LiveData<WeatherModel> loadWeather(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<WeatherModel> weathers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(WeatherModel weather);
}
