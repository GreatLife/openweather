package com.thanhnt.openweather.db.model;

import android.arch.persistence.room.TypeConverters;

import com.thanhnt.openweather.db.DateConverter;

import java.util.Date;

/**
 * Created by xedap on 5/10/2018.
 */

public class Sys {
    public int type;
    public long id;
    public String message;
    public String country;
    //    @TypeConverters({DateConverter.class})
//    public Date sunrise;
//    @TypeConverters({DateConverter.class})
//    public Date sunset;
    public long sunrise;
    public long sunset;
}
