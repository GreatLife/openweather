package com.thanhnt.openweather.db.model;

/**
 * Created by xedap on 5/10/2018.
 */

public class Weather {
    public long id;
    public String main;
    public String description;
    public String icon;

    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", main='" + main + '\'' +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
