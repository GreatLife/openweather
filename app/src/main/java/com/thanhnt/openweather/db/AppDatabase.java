package com.thanhnt.openweather.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.support.annotation.NonNull;

import com.thanhnt.openweather.db.dao.WeatherDao;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.util.AppExecutors;

@Database(entities = {WeatherModel.class}, version = 1)
@TypeConverters({DateConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "weather_db";
    private static AppDatabase INSTANCE;

    public abstract WeatherDao weatherDao();

    public static synchronized AppDatabase getInstance(Context context, AppExecutors appExecutors) {
        if (INSTANCE == null) {
            INSTANCE = buildDatabase(context, appExecutors);
        }
        return INSTANCE;
    }

    private static AppDatabase buildDatabase(Context context, AppExecutors appExecutors) {
        return Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }
                })
                .build();
    }
}
