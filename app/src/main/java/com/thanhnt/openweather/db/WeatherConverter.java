package com.thanhnt.openweather.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thanhnt.openweather.db.model.Weather;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherConverter {
    @TypeConverter
    public static List<Weather> fromString(String value) {
        Type listType = new TypeToken<List<Weather>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(List<Weather> list) {
        return new Gson().toJson(list);
    }
}
