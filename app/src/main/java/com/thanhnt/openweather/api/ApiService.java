package com.thanhnt.openweather.api;

import android.arch.lifecycle.LiveData;

import com.thanhnt.openweather.db.model.WeatherList;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.util.Constants;

import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("group?id=2643743,4548393,5391959&units=metric&appid=" + Constants.API_KEY)
    LiveData<ApiResponse<WeatherList>> getWeathers(@Query("lang") String lang);

    @GET("weather?units=metric&appid=" + Constants.API_KEY)
    LiveData<ApiResponse<WeatherModel>> getWeather(@Query("id") long id, @Query("lang") String lang);
}
