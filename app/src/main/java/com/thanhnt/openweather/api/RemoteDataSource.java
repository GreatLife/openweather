package com.thanhnt.openweather.api;

import android.arch.lifecycle.LiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thanhnt.openweather.db.model.WeatherList;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.util.Constants;
import com.thanhnt.openweather.util.LiveDataCallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteDataSource implements ApiService {
    private static RemoteDataSource INSTANCE;
    private ApiService apiService;

    private RemoteDataSource() {

    }

    private void initApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public static synchronized RemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDataSource();
            INSTANCE.initApi();
        }
        return INSTANCE;
    }

    @Override
    public LiveData<ApiResponse<WeatherList>> getWeathers(String lang) {
        return apiService.getWeathers(lang);
    }

    @Override
    public LiveData<ApiResponse<WeatherModel>> getWeather(long id, String lang) {
        return apiService.getWeather(id, lang);
    }
}
