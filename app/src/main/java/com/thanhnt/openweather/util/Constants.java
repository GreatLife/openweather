package com.thanhnt.openweather.util;

/**
 * Created by xedap on 5/10/2018.
 */

public interface Constants {

    String EXTRA_ID = "id";
    String EXTRA_NAME = "name";

    String API_KEY = "e8e63da228d8a650ead5f108fdd0dda1";
    String HOST = "https://api.openweathermap.org/data/2.5/";
}
