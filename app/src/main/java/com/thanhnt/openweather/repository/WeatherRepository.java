package com.thanhnt.openweather.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.thanhnt.openweather.R;
import com.thanhnt.openweather.api.ApiResponse;
import com.thanhnt.openweather.api.NetworkBoundResource;
import com.thanhnt.openweather.api.RemoteDataSource;
import com.thanhnt.openweather.api.Resource;
import com.thanhnt.openweather.db.AppDatabase;
import com.thanhnt.openweather.db.dao.WeatherDao;
import com.thanhnt.openweather.db.model.WeatherList;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.util.AppExecutors;

import java.util.List;

/**
 * Created by xedap on 1/13/2018.
 */

public class WeatherRepository {
    private static WeatherRepository INSTANCE;
    private AppExecutors appExecutors;
    private WeatherDao weatherDao;
    private RemoteDataSource remoteDataSource;
    private String lang = "en";

    private WeatherRepository(Context context) {
        appExecutors = AppExecutors.getInstance();
        weatherDao = AppDatabase.getInstance(context, appExecutors).weatherDao();
        remoteDataSource = RemoteDataSource.getInstance();
        lang = context.getResources().getString(R.string.lang);
    }

    public static synchronized WeatherRepository getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(context);
        }
        return INSTANCE;
    }

    public LiveData<Resource<List<WeatherModel>>> loadWeathers() {
        Log.d("MainActivity", "load weathers");
        return new NetworkBoundResource<List<WeatherModel>, WeatherList>(appExecutors) {
            @Override
            protected void saveCallResult(@NonNull WeatherList item) {
                if (item.list != null) {
                    Log.d("MainActivity", "saved to db");
                    weatherDao.insertAll(item.list);
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<WeatherModel> data) {
                Log.d("MainActivity", "shouldFetch to db");
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<WeatherModel>> loadFromDb() {
                Log.d("MainActivity", "loadFromDb to db");
                return weatherDao.loadWeathers();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<WeatherList>> createCall() {
                Log.d("MainActivity", "createCall to db");
                return remoteDataSource.getWeathers(lang);
            }
        }.asLiveData();
    }

    public LiveData<Resource<WeatherModel>> loadWeather(long id) {
        return new NetworkBoundResource<WeatherModel, WeatherModel>(appExecutors) {

            @Override
            protected void saveCallResult(@NonNull WeatherModel item) {
                if (item != null && item.id > 0) {
                    weatherDao.insert(item);
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable WeatherModel data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<WeatherModel> loadFromDb() {
                return weatherDao.loadWeather(id);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<WeatherModel>> createCall() {
                return remoteDataSource.getWeather(id, lang);
            }
        }.asLiveData();
    }

}
