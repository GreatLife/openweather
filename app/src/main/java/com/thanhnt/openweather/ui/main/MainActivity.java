package com.thanhnt.openweather.ui.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.thanhnt.openweather.R;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.ui.detail.WeatherDetailActivity;
import com.thanhnt.openweather.ui.detail.WeatherDetailFragment;
import com.thanhnt.openweather.util.Constants;

public class MainActivity extends AppCompatActivity implements WeatherAdapter.OnWeatherItemClickListener,
        RefreshButtonFragment.OnRefreshButtonClickListener {

    private static final int PERMISSION_REQUEST_CODE = 1111;
    private static final String TAG_WEATHER_LIST = "weather_list";
    private static final String TAG_WEATHER_DETAIL = "weather_detail";
    private static final String TAG_REFRESH = "refresh";

    private Fragment mWeatherListFragment;
    private Fragment mWeatherDetailFragment;
    private long id;

    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean isPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isPhone = getResources().getBoolean(R.bool.is_phone);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment refreshButtonFragment = getSupportFragmentManager().findFragmentByTag(TAG_REFRESH);

        if (refreshButtonFragment == null) {
            refreshButtonFragment = new RefreshButtonFragment();
        }
        if (!refreshButtonFragment.isAdded()) {
            fragmentTransaction.add(R.id.refresh_fragment_container, refreshButtonFragment, TAG_REFRESH);
        }

        mWeatherListFragment = getSupportFragmentManager().findFragmentByTag(TAG_WEATHER_LIST);
        if (mWeatherListFragment == null) {
            mWeatherListFragment = new WeatherListFragment();
        }
        if (!mWeatherListFragment.isAdded()) {
            fragmentTransaction.add(R.id.content_fragment_container, mWeatherListFragment, TAG_WEATHER_LIST);
        }

        // Handle tablet (if have time)
        if (!isPhone) {
            id = -1;
            if (savedInstanceState != null) {
                id = savedInstanceState.getLong(Constants.EXTRA_ID, -1);
            }

            mWeatherDetailFragment = getSupportFragmentManager().findFragmentByTag(TAG_WEATHER_DETAIL);
            if (mWeatherDetailFragment == null) {
                mWeatherDetailFragment = new WeatherDetailFragment();
            }
            if (!mWeatherDetailFragment.isAdded()) {
                Bundle bundle = new Bundle();
                bundle.putLong(Constants.EXTRA_ID, id);
                mWeatherDetailFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.content_fragment_container, mWeatherDetailFragment, TAG_WEATHER_DETAIL);
            }
        }

        fragmentTransaction.commitAllowingStateLoss();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    @Override
    public void onItemClick(WeatherModel weatherModel) {
        if (isPhone) {
            Intent intent = new Intent(this, WeatherDetailActivity.class);
            intent.putExtra(Constants.EXTRA_ID, weatherModel.id);
            intent.putExtra(Constants.EXTRA_NAME, weatherModel.name);
            startActivity(intent);
        } else {

        }
    }

    @Override
    public void onRefresh() {
        if (mWeatherListFragment != null && mWeatherListFragment instanceof WeatherListFragment) {
            ((WeatherListFragment) mWeatherListFragment).refresh();
            logEvent("2222", "refresh", "button");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mnuPermission) {
            if (requestPermission(PERMISSION_REQUEST_CODE)) {
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean requestPermission(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            boolean granted = true;
            for (int gr : grantResults) {
                if (gr != PackageManager.PERMISSION_GRANTED) {
                    granted = false;
                    break;
                }
            }
            if (granted) {
                Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    //    Firebase analytics demo
    private void logEvent(String id, String name, String contentType) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
