package com.thanhnt.openweather.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thanhnt.openweather.R;

/**
 * Created by xedap on 5/10/2018.
 */

public class RefreshButtonFragment extends Fragment implements View.OnClickListener {
    private OnRefreshButtonClickListener mOnRefreshButtonClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRefreshButtonClickListener) {
            mOnRefreshButtonClickListener = (OnRefreshButtonClickListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refresh_button, container, false);
        view.findViewById(R.id.btnRefresh).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnRefresh) {
            if (mOnRefreshButtonClickListener != null) {
                mOnRefreshButtonClickListener.onRefresh();
            }
        }
    }

    public interface OnRefreshButtonClickListener {
        void onRefresh();
    }
}
