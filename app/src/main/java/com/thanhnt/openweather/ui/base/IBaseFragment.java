package com.thanhnt.openweather.ui.base;

import android.content.Context;

public interface IBaseFragment {
    void showToastMessage(String message);
    void showToastMessage(int messageRes);
    void setProgressBar(boolean show);
    void showSnackbar(String message);
    Context getContext();
}
