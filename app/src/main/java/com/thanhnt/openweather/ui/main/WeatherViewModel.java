package com.thanhnt.openweather.ui.main;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;

import com.thanhnt.openweather.api.Resource;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.repository.WeatherRepository;

import java.util.List;


public class WeatherViewModel extends AndroidViewModel {
    private LiveData<Resource<List<WeatherModel>>> mWeathers;

    public WeatherViewModel(@NonNull Application application) {
        super(application);
//        mWeathers = WeatherRepository.getInstance(application.getApplicationContext()).loadWeathers();
    }

    public LiveData<Resource<List<WeatherModel>>> getWeathers(Context context) {
        mWeathers = WeatherRepository.getInstance(context).loadWeathers();
        return mWeathers;
    }

    public LiveData<Resource<List<WeatherModel>>> getWeathers() {
        return mWeathers;
    }
}
