package com.thanhnt.openweather.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thanhnt.openweather.R;
import com.thanhnt.openweather.api.Resource;
import com.thanhnt.openweather.api.Status;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.ui.base.BaseFragment;
import com.thanhnt.openweather.util.Utils;

import java.util.List;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherListFragment extends BaseFragment {
    private WeatherAdapter mWeatherAdapter;
    private WeatherAdapter.OnWeatherItemClickListener mOnWeatherItemClickListener;
    private RecyclerView rvWeatherList;
    private TextView tvNoItems;
    private WeatherViewModel mWeatherViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WeatherAdapter.OnWeatherItemClickListener) {
            mOnWeatherItemClickListener = (WeatherAdapter.OnWeatherItemClickListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_list, container, false);
        progressBar = view.findViewById(R.id.progressbar);
        tvNoItems = view.findViewById(R.id.tvNoItems);
        rvWeatherList = view.findViewById(R.id.rvWeatherList);
        rvWeatherList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mWeatherAdapter = new WeatherAdapter(getActivity());
        mWeatherAdapter.setOnWeatherItemClickListener(mOnWeatherItemClickListener);
        rvWeatherList.setAdapter(mWeatherAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() == null) {
            return;
        }
        mWeatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);
        mWeatherViewModel.getWeathers(getActivity().getApplicationContext()).observe(this, mObserver);
    }

    private Observer<Resource<List<WeatherModel>>> mObserver = this::handleResponse;

    private void handleResponse(@Nullable Resource<List<WeatherModel>> listResource) {
        if (listResource == null) {
            return;
        }
        if (listResource.status == Status.LOADING) {
            setProgressBar(true);
        } else {
            setProgressBar(false);
        }

        if (listResource.status != Status.LOADING) {
            if (listResource.data != null && listResource.data.size() > 0) {
                tvNoItems.setVisibility(View.GONE);
                rvWeatherList.setVisibility(View.VISIBLE);
                mWeatherAdapter.update(listResource.data);
                mWeatherAdapter.notifyDataSetChanged();
            }
        }

        if (listResource.status == Status.ERROR && (listResource.data == null || listResource.data.isEmpty())) {
            rvWeatherList.setVisibility(View.GONE);
            tvNoItems.setVisibility(View.VISIBLE);
            showToastMessage(R.string.warning_message);
        }

        if (listResource.status == Status.ERROR
                && getActivity() != null
                && !Utils.isNetworkConnected(getActivity())) {
            showToastMessage(R.string.warning_message);
        }
    }

    public void refresh() {
        if (mWeatherViewModel.getWeathers() != null && getActivity() != null) {
            mWeatherViewModel.getWeathers().removeObserver(mObserver);
            mWeatherViewModel.getWeathers(getActivity().getApplicationContext()).observe(this, mObserver);
        }
    }
}
