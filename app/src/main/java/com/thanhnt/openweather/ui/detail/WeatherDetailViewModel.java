package com.thanhnt.openweather.ui.detail;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;

import com.thanhnt.openweather.api.Resource;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.repository.WeatherRepository;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherDetailViewModel extends AndroidViewModel {
    private LiveData<Resource<WeatherModel>> mWeather;

    public WeatherDetailViewModel(@NonNull Application application, long id) {
        super(application);
    }

    public LiveData<Resource<WeatherModel>> getWeather(Context context, long id) {
        mWeather = WeatherRepository.getInstance(context).loadWeather(id);
        return mWeather;
    }

    public LiveData<Resource<WeatherModel>> getWeather() {
        return mWeather;
    }
}