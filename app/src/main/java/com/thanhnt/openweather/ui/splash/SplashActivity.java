package com.thanhnt.openweather.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.thanhnt.openweather.BuildConfig;
import com.thanhnt.openweather.R;
import com.thanhnt.openweather.ui.main.MainActivity;

/**
 * Created by xedap on 5/11/2018.
 */

public class SplashActivity extends AppCompatActivity {
    private static final long SPLASH_TIME = 2000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView ivSplash = findViewById(R.id.ivSplash);
        TextView tvVersion = findViewById(R.id.tvVersion);

        if (BuildConfig.IS_PRO) {
            ivSplash.setImageResource(R.drawable.rain);
            tvVersion.setText(R.string.production);
        } else {
            ivSplash.setImageResource(R.drawable.sun);
            tvVersion.setText(R.string.development);
        }

        new Handler().postDelayed(this::startMain, SPLASH_TIME);
    }

    private void startMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
