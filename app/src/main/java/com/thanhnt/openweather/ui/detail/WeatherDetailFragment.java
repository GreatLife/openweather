package com.thanhnt.openweather.ui.detail;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.thanhnt.openweather.R;
import com.thanhnt.openweather.api.Resource;
import com.thanhnt.openweather.api.Status;
import com.thanhnt.openweather.db.model.WeatherModel;
import com.thanhnt.openweather.ui.base.BaseFragment;
import com.thanhnt.openweather.util.Utils;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherDetailFragment extends BaseFragment {
    private TextView tvName;
    private ImageView ivIcon;
    private TextView tvTemp;
    private TextView tvWeather;
    private TextView tvWind;
    private TextView tvPressure;
    private TextView tvHumidity;
    private TextView tvSunrise;
    private TextView tvSunset;
    private TextView tvGeoCoords;

    private WeatherDetailViewModel mWeatherDetailViewModel;
    private long id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        progressBar = view.findViewById(R.id.progressbar);
        tvName = view.findViewById(R.id.tvName);
        ivIcon = view.findViewById(R.id.ivIcon);
        tvTemp = view.findViewById(R.id.tvTemp);
        tvWeather = view.findViewById(R.id.tvWeather);
        tvWind = view.findViewById(R.id.tvWind);
        tvPressure = view.findViewById(R.id.tvPressure);
        tvHumidity = view.findViewById(R.id.tvHumidity);
        tvSunrise = view.findViewById(R.id.tvSunrise);
        tvSunset = view.findViewById(R.id.tvSunset);
        tvGeoCoords = view.findViewById(R.id.tvGeoCoords);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() == null) {
            return;
        }
        id = -1;
        if (getArguments() != null) {
            id = getArguments().getLong("id", -1);
        }
        mWeatherDetailViewModel = ViewModelProviders.of(this,
                new WeatherDetailFactory(getActivity().getApplication(), id))
                .get(WeatherDetailViewModel.class);
        mWeatherDetailViewModel.getWeather(getActivity().getApplicationContext(), id).observe(this, mObserver);
    }

    private Observer<Resource<WeatherModel>> mObserver = this::handleResponse;

    private void handleResponse(Resource<WeatherModel> weatherModelResource) {
        if (weatherModelResource == null) {
            return;
        }
        if (weatherModelResource.status == Status.LOADING) {
            setProgressBar(true);
        } else {
            setProgressBar(false);
        }

        if (weatherModelResource.status != Status.LOADING) {
            if (weatherModelResource.data != null) {
                buildUi(weatherModelResource.data);
            }
        }

        if (weatherModelResource.status == Status.ERROR
                && getActivity() != null
                && !Utils.isNetworkConnected(getActivity())) {
            showToastMessage(R.string.warning_message);
        }
    }

    @SuppressLint("DefaultLocale")
    private void buildUi(WeatherModel weatherModel) {
        tvName.setText(getString(R.string.text_name, weatherModel.name));
        if (getContext() != null) {
            Glide.with(getContext())
                    .load("https://openweathermap.org/img/w/" + weatherModel.weather.get(0).icon + ".png")
                    .into(ivIcon);
        }
        tvTemp.setText(getString(R.string.text_temp, Math.round(weatherModel.main.temp)));
        tvWeather.setText(getString(R.string.text_weather, weatherModel.weather.get(0).description));
        tvWind.setText(getString(R.string.text_wind, weatherModel.wind.speed, Math.round(weatherModel.wind.deg)));
        tvHumidity.setText(TextUtils.concat(String.valueOf(Math.round(weatherModel.main.humidity)), " %"));
        tvPressure.setText(getString(R.string.text_pressure, Math.round(weatherModel.main.pressure)));
        tvSunrise.setText(Utils.getTimeString(weatherModel.sys.sunrise));
        tvSunset.setText(Utils.getTimeString(weatherModel.sys.sunset));
        tvGeoCoords.setText(getString(R.string.text_geo_coords, weatherModel.coord.lat, weatherModel.coord.lon));
    }

    public void refresh() {
        if (mWeatherDetailViewModel.getWeather() != null && getActivity() != null) {
            mWeatherDetailViewModel.getWeather().removeObserver(mObserver);
            mWeatherDetailViewModel.getWeather(getActivity().getApplicationContext(), id).observe(this, mObserver);
        }
    }
}
