package com.thanhnt.openweather.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thanhnt.openweather.R;
import com.thanhnt.openweather.db.model.WeatherModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {
    private Context mContext;
    private List<WeatherModel> mWeatherModels;
    private OnWeatherItemClickListener mOnWeatherItemClickListener;

    public WeatherAdapter(Context context) {
        this.mContext = context;
        this.mWeatherModels = new ArrayList<>();
    }

    public void update(List<WeatherModel> weatherModels) {
        this.mWeatherModels = weatherModels;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_weather_item, parent, false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        WeatherModel weatherModel = mWeatherModels.get(holder.getAdapterPosition());
        holder.tvName.setText(weatherModel.name);
        holder.tvWeather.setText(mContext.getString(R.string.text_weather_list, weatherModel.weather.get(0).description));
        holder.tvTemp.setText(mContext.getResources().getString(R.string.text_temp_list, Math.round(weatherModel.main.temp)));
        if (position == mWeatherModels.size() - 1) {
            holder.divider.setVisibility(View.INVISIBLE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mWeatherModels != null ? mWeatherModels.size() : 0;
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        TextView tvWeather;
        TextView tvTemp;
        View divider;

        public WeatherViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvWeather = itemView.findViewById(R.id.tvWeather);
            tvTemp = itemView.findViewById(R.id.tvTemp);
            divider = itemView.findViewById(R.id.divider);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnWeatherItemClickListener != null) {
                mOnWeatherItemClickListener.onItemClick(mWeatherModels.get(getAdapterPosition()));
            }
        }
    }

    public void setOnWeatherItemClickListener(OnWeatherItemClickListener onWeatherItemClickListener) {
        this.mOnWeatherItemClickListener = onWeatherItemClickListener;
    }

    public interface OnWeatherItemClickListener {
        void onItemClick(WeatherModel weatherModel);
    }
}
