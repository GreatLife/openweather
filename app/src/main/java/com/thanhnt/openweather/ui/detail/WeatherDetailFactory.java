package com.thanhnt.openweather.ui.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

public class WeatherDetailFactory extends ViewModelProvider.NewInstanceFactory {
    private Application application;
    private long id;

    WeatherDetailFactory(Application application, long id) {
        this.application = application;
        this.id = id;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new WeatherDetailViewModel(application, id);
    }
}