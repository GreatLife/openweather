package com.thanhnt.openweather.ui.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;

import com.thanhnt.openweather.R;
import com.thanhnt.openweather.ui.main.RefreshButtonFragment;
import com.thanhnt.openweather.util.Constants;

/**
 * Created by xedap on 5/10/2018.
 */

public class WeatherDetailActivity extends AppCompatActivity implements RefreshButtonFragment.OnRefreshButtonClickListener {
    private static final String TAG_WEATHER_DETAIL = "weather_detail";
    private static final String TAG_REFRESH = "refresh";

    private Fragment mWeatherDetailFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        long id = -1;
        String name = "";
        if (getIntent() != null) {
            id = getIntent().getLongExtra(Constants.EXTRA_ID, -1);
            name = getIntent().getStringExtra(Constants.EXTRA_NAME);
        }

        if (TextUtils.isEmpty(name)) {
             name = getResources().getString(R.string.app_name);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(name);
        }



        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment refreshButtonFragment = getSupportFragmentManager().findFragmentByTag(TAG_REFRESH);

        if (refreshButtonFragment == null) {
            refreshButtonFragment = new RefreshButtonFragment();
        }
        if (!refreshButtonFragment.isAdded()) {
            fragmentTransaction.add(R.id.refresh_fragment_container, refreshButtonFragment, TAG_REFRESH);
        }

        mWeatherDetailFragment = getSupportFragmentManager().findFragmentByTag(TAG_WEATHER_DETAIL);
        if (mWeatherDetailFragment == null) {
            mWeatherDetailFragment = new WeatherDetailFragment();
        }
        if (!mWeatherDetailFragment.isAdded()) {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.EXTRA_ID, id);
            mWeatherDetailFragment.setArguments(bundle);
            fragmentTransaction.add(R.id.content_fragment_container, mWeatherDetailFragment, TAG_WEATHER_DETAIL);
        }

        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if (mWeatherDetailFragment != null && mWeatherDetailFragment instanceof WeatherDetailFragment) {
            ((WeatherDetailFragment) mWeatherDetailFragment).refresh();
        }
    }
}
